class Employee < ApplicationRecord
  belongs_to :manager, class_name: 'Employee'
  has_many :appoinments
  has_many :customers, through: :appoinments
  has_many :subordinates, class_name: 'Employee', foreign_key: 'manager_id'
end

class Detail < ApplicationRecord
  belongs_to :invoice
  belongs_to :book
end

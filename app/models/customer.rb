class Customer < ApplicationRecord
  belongs_to :country
  belongs_to :user, optional: true
  has_many :invoices, dependent: :destroy
  has_many :appoinments, dependent: :destroy
  has_many :employees, through: :appoinments

  def self.customer_with_invoices
    joins(invoices: :details)
  end
end

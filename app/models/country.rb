class Country < ApplicationRecord
  has_many :customers, dependent: :destroy
  has_many :publishers, dependent: :destroy
  validates :name, presence: true
end

class Payment < ApplicationRecord
  enum kind: { cash: 0, debit_card: 1 }

  belongs_to :invoice
  has_many :assets, as: :attachable
end

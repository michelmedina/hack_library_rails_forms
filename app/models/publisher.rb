class Publisher < ApplicationRecord
  belongs_to :country
  has_many :books, dependent: :destroy
end

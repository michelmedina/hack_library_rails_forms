class Book < ApplicationRecord
  belongs_to :publisher
  has_many :assets, as: :attachable
  has_many :details
  has_and_belongs_to_many :warehouses

  def self.list
    Book.includes(:publisher).map { |book|  book.list_of_publishers }
  end

  def self.find_title(title, author)
    where("title = ? and author = ?", title, author)
  end

  def list_of_publishers
    "Book: #{title} | Publisher: #{publisher.name}"
  end
end

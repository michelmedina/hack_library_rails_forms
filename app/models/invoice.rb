class Invoice < ApplicationRecord
  belongs_to :customer
  has_many :orders
  has_many :appointments, through: :orders
  has_many :details, dependent: :destroy
  has_many :assets, as: :attachable

  after_create :calculate_customer_statement

  scope :paid, -> { where(is_paid: 'true') }
  scope :created_before, ->(time) { where('date < ?', time) }

  private

  def calculate_customer_statement
    # Coolz stuff
  end
end

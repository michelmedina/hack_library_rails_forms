class BooksController < ApplicationController

  def index
    @publisher = Publisher.find_by(id: params[:publisher_id])
    @books = @publisher.books
    @books
  end
end

class InvoiceSearchesController < ApplicationController
  def index
    Invoice.where('date < ?', params[:date])
  end
end

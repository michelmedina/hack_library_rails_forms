Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :countries
  resources :customers
  resources :employees
  resources :appoinments
  resources :orders
  resources :wharehouse do
    member do
      get :books, to: 'wharehouse_books#index'
      post :books, to: 'wharehouse_books#create'
    end
  end

  resources :publishers, shallow: true do
    resources :books do
      member do
        get :assets, to: 'book_assets#index'
        post :assets, to: 'book_assets#create'
      end
    end
  end

  resources :invoices, shallow: true do
    collection do
      get :search, to: 'invoice_searches#index'
    end
    resources :payments do
      member do
        get :assets, to: 'payment_assets#index'
        post :assets, to: 'payment_assets#create'
      end
    end
  end
end

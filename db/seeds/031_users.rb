# frozen_string_literal: true

10.times do
  User.create(email: Faker::Internet.email, password: Faker::Internet.password)
end

countries = Country.all
User.all.each do |user|
  Customer.create do |customer|
    customer.name     = Faker::Name.name
    customer.address  = Faker::Address.full_address
    customer.phone    = Faker::PhoneNumber.cell_phone if rand(1..100).even?
    customer.country  = countries.sample
    customer.user     = user
  end
end

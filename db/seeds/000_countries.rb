# frozen_string_literal: true

10.times do
  Country.create(name: Faker::Address.country)
end

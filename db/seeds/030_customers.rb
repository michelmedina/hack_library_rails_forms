# frozen_string_literal: true

countries = Country.all
30.times do
  Customer.create do |customer|
    customer.name     = Faker::Name.name
    customer.address  = Faker::Address.full_address
    customer.phone    = Faker::PhoneNumber.cell_phone
    customer.country  = countries.sample
  end
end

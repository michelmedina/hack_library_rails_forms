# frozen_string_literal: true

3.times do
  Warehouse.create(code: Faker::Code.sin)
end

warehouses = Warehouse.all
max_index = warehouses.size - 1
books = Book.all.to_a

books.size.times do
  sample = books.sample
  warehouses[rand(0..max_index)].books << sample
  books.delete_at(books.index(sample))
end

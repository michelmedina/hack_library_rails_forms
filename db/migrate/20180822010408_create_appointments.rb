class CreateAppointments < ActiveRecord::Migration[5.2]
  def change
    create_table :appointments do |t|
      t.date :date
      t.references :customer, foreign_key: true
      t.references :employee, foreign_key: true
      t.timestamps
    end
  end
end

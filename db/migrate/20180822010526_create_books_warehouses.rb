class CreateBooksWarehouses < ActiveRecord::Migration[5.2]
  def change
    create_join_table :books, :warehouses do |t|
      t.index :book_id
      t.index :warehouse_id
      t.timestamps
    end
  end
end

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_09_11_021729) do

  create_table "appointments", force: :cascade do |t|
    t.date "date"
    t.integer "customer_id"
    t.integer "employee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["customer_id"], name: "index_appointments_on_customer_id"
    t.index ["employee_id"], name: "index_appointments_on_employee_id"
  end

  create_table "assets", force: :cascade do |t|
    t.string "name"
    t.string "url"
    t.string "attachable_type"
    t.integer "attachable_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["attachable_type", "attachable_id"], name: "index_assets_on_attachable_type_and_attachable_id"
  end

  create_table "books", force: :cascade do |t|
    t.string "title"
    t.string "author"
    t.decimal "price", precision: 5, scale: 2
    t.integer "publisher_id"
    t.index ["publisher_id"], name: "index_books_on_publisher_id"
  end

  create_table "books_warehouses", id: false, force: :cascade do |t|
    t.integer "book_id", null: false
    t.integer "warehouse_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["book_id"], name: "index_books_warehouses_on_book_id"
    t.index ["warehouse_id"], name: "index_books_warehouses_on_warehouse_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
  end

  create_table "customers", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.string "phone"
    t.decimal "paid", precision: 5, scale: 2
    t.decimal "debt", precision: 5, scale: 2
    t.integer "country_id"
    t.integer "user_id"
    t.index ["country_id"], name: "index_customers_on_country_id"
    t.index ["user_id"], name: "index_customers_on_user_id"
  end

  create_table "details", force: :cascade do |t|
    t.integer "quantity"
    t.integer "book_id"
    t.integer "invoice_id"
    t.decimal "price", precision: 5, scale: 2
    t.index ["book_id"], name: "index_details_on_book_id"
    t.index ["invoice_id"], name: "index_details_on_invoice_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "manager_id"
    t.index ["manager_id"], name: "index_employees_on_manager_id"
  end

  create_table "invoices", force: :cascade do |t|
    t.date "date"
    t.boolean "is_paid"
    t.integer "customer_id"
    t.index ["customer_id"], name: "index_invoices_on_customer_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "code"
    t.integer "invoice_id"
    t.integer "appoinment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["appoinment_id"], name: "index_orders_on_appoinment_id"
    t.index ["invoice_id"], name: "index_orders_on_invoice_id"
  end

  create_table "payments", force: :cascade do |t|
    t.decimal "amount"
    t.integer "kind"
    t.integer "invoice_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["invoice_id"], name: "index_payments_on_invoice_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "body"
    t.boolean "published"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "publishers", force: :cascade do |t|
    t.string "name"
    t.integer "country_id"
    t.index ["country_id"], name: "index_publishers_on_country_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email"
    t.string "password"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "warehouses", force: :cascade do |t|
    t.string "code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
